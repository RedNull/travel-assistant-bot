import json
import logging
import math

from redis import Redis
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Bot

import utils
from utils import log

from . import constants

logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")
LOGGER = logging.getLogger('bot_service')

redis_server = Redis('redis')


def query_handler(bot, update):
    chat_id = update.callback_query.message.chat_id
    message_id = update.callback_query.message.message_id
    keyboard_data = update.callback_query.data

    user_id = update.callback_query.message.chat.id
    redis_list = get_info_from_redis(user_id)
    flight_num = utils.get_flight_number(redis_server, user_id)

    for item in redis_list:
        if keyboard_data.split('.')[1] in item:
            task_status = item[keyboard_data.split('.')[1]]
            item[keyboard_data.split('.')[1]] = not task_status

    redis_server.set(f'todo:{flight_num}', json.dumps(redis_list))

    reply_markup = InlineKeyboardMarkup(generate_keyboard(redis_list))

    bot.editMessageText(generate_list(redis_list), chat_id=chat_id, message_id=message_id,
                        reply_markup=reply_markup, parse_mode='html')


@log
def todo(bot: Bot, update, chat_id=None):
    user_id = update.message.from_user.id if not chat_id else chat_id
    todo_list = get_info_from_redis(user_id)

    generate_keyboard(todo_list)
    generate_list(todo_list)

    reply_markup = InlineKeyboardMarkup(generate_keyboard(todo_list))
    bot.send_message(user_id, generate_list(todo_list), reply_markup=reply_markup)


def get_info_from_redis(user_id):
    flight_num = utils.get_flight_number(redis_server, user_id)
    flight_to_do_list = redis_server.get(f'todo:{flight_num}')

    if flight_to_do_list:
        return json.loads(flight_to_do_list)
    else:
        to_do = create_todo_list()
        redis_server.set(f'todo:{flight_num}', json.dumps(to_do))

        return to_do


def create_todo_list():
    todo_list = [
        'Взять визу',
        'Взять паспорт',
        'Забронировать отель',
        'Проверить страховой полис',
        'Лекарства',
        'Очки',
        'Зарядка для телефона',
        'Переходник для розетки',
        'Зубная паста',
        'Зубная щетка',
        'Нижнее белье',
        'Верхняя одежда',
        'Хорошее настроение'
    ]

    return [{task: False} for task in todo_list]


def generate_list(todo_list):
    compiled_list = []

    task_number = zip(todo_list, range(1, len(todo_list) + 1))

    for task, num in task_number:
        for task_title in task:
            if task[task_title]:
                compiled_list.append(f'{constants.DONE_ICON}  {num}.{task_title}')
            else:
                compiled_list.append(f'{constants.UNDONE_ICON}  {num}.{task_title}')

    return '\n'.join(compiled_list)


def generate_keyboard(todo_list):
    list_copy = todo_list.copy()
    todo_keyboard = []

    list_length = len(todo_list)
    rows_num = math.ceil(list_length / constants.KEYS_PER_ROW)

    for row in range(0, rows_num):
        keys_row = []
        for i in range(0, constants.KEYS_PER_ROW):
            if list_copy:
                item = list_copy.pop(0)
                for title in item:
                    data = f'todo:{todo_list.index(item) + 1}.{title}'
                    if item[title]:
                        keys_row.append(
                            InlineKeyboardButton(f'{todo_list.index(item) + 1} {constants.UNDONE_ICON}',
                                                 callback_data=data))
                    else:
                        keys_row.append(InlineKeyboardButton(f'{todo_list.index(item) + 1} {constants.DONE_ICON}',
                                                             callback_data=data))
            else:
                break

        todo_keyboard.append(keys_row)

    return todo_keyboard
