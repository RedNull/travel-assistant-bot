import random
import time

import redis
from telegram import (InlineKeyboardButton, InlineKeyboardMarkup, Bot)

from utils import log

r = redis.Redis(host='redis', port=6379, db=0)


def query_handler(bot, update):
    query = update.callback_query
    chat_id = query.message.chat.id

    if query.data == 'init':
        return online_checkin_handler(bot, None, chat_id)

    if query.data in ['first_part', 'middle_part', 'last_part']:
        return seat_row_handler(bot, chat_id, query.data)

    elif query.data in ['window', 'middle', 'aisle']:
        return seats_handler(bot, chat_id, query.data)


def is_already_checked_in(bot, chat_id):
    checkin_status = r.get(f'{chat_id}:checkin_status') or b''
    if checkin_status.decode() == 'done':
        bot.send_message(chat_id=chat_id, text='Вы уже зарегистрированы на рейс.')
        return True

    return False


@log
def online_checkin_handler(bot, update, chat_id=None):
    if not chat_id:
        chat_id = update.message.chat.id

    if is_already_checked_in(bot, chat_id):
        return

    custom_keyboard = [(
        InlineKeyboardButton('Носовая часть', callback_data='checkin:first_part'),
        InlineKeyboardButton('Середина', callback_data='checkin:middle_part'),
        InlineKeyboardButton('Хвостовая часть', callback_data='checkin:last_part'),
    )]
    reply_markup = InlineKeyboardMarkup(custom_keyboard, resize_keyboard=False)
    bot.send_message(chat_id, 'Выберите часть самолета, в которой Вы бы хотели сидеть',
                     reply_markup=reply_markup)


@log
def seat_row_handler(bot: Bot, chat_id: int, command: str):
    if is_already_checked_in(bot, chat_id):
        return

    seat_row = random.randint(0, 100)

    if command in ['first_part']:
        seat_row = random.randint(0, 30)

    elif command in ['middle_part']:
        seat_row = random.randint(31, 60)

    elif command in ['last_part']:
        seat_row = random.randint(61, 100)

    else:
        bot.send_message(chat_id, 'Выбранная часть самолета не распознана')

    r.set('seat_row', seat_row)

    custom_keyboard = [(
        InlineKeyboardButton('У окна', callback_data='checkin:window'),
        InlineKeyboardButton('Посередине', callback_data='checkin:middle'),
        InlineKeyboardButton('У прохода', callback_data='checkin:aisle'),
    )]
    reply_markup = InlineKeyboardMarkup(custom_keyboard, resize_keyboard=False)
    bot.send_message(chat_id=chat_id, text='Выберите место',
                     reply_markup=reply_markup)


@log
def seats_handler(bot: Bot, chat_id: int, desired_seat_position: str):
    if is_already_checked_in(bot, chat_id):
        return

    seat = random.choice(['A', 'B', 'C', 'D', 'E', 'F'])

    if desired_seat_position in ['window']:
        seat = random.choice(['A', 'F'])

    elif desired_seat_position in ['middle']:
        seat = random.choice(['B', 'E'])

    elif desired_seat_position in ['aisle']:
        seat = random.choice(['C', 'D'])

    else:
        bot.send_message(chat_id=chat_id, text='Выбранное место не распознано.')

    bot.send_message(chat_id,
                     'Мы пытаемся зарегистрировать Вас на рейс.'
                     'Пожалуйста подождите...')
    time.sleep(5)

    seat_row = r.get("seat_row").decode('UTF-8')
    bot.send_message(chat_id, f'Зарегистрированные места: {seat_row}{seat}')

    root_folder = '/usr/src/app/'
    bot.send_photo(chat_id=chat_id, photo=open(f'{root_folder}static/boarding_pass.jpg', 'rb'))

    r.set(f'{chat_id}:checkin_status', 'done', ex=120)
