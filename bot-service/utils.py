import functools
import logging

from constants import MAIN_MENU_STATUS_CODE

logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")

user_state = dict()
registry = dict()


def log(func, name=None):
    def wrapper(*args, **kwargs):
        logger = logging.getLogger(name or 'bot_service')
        logger.info(f'Request: {func.__name__}')
        func(*args, **kwargs)

    return wrapper


def state_start(state: int):

    def decorator(fn):

        @functools.wraps(fn)
        def wrapper(bot, update, *args, **kwargs):
            if update.callback_query:
                user_id = update.callback_query.data.split(':')[-1]
            else:
                user_id = update.message.from_user.id

            fn(bot, update, *args, **kwargs)
            user_state[str(user_id)] = state

        return wrapper

    return decorator


def state_end(state: int):

    def decorator(fn):
        if state not in registry:
            registry[state] = fn

        @functools.wraps(fn)
        def wrapper(bot, update, *args, **kwargs):  # todo fix not called on received_flight
            user_id = str(update.message.from_user.id)
            fn(bot, update, *args, **kwargs)
            if user_id in user_state:
                del user_state[user_id]

        return wrapper

    return decorator


def perform_state_handlers(bot, update):
    user_id = update.message.from_user.id
    state = user_state.get(str(user_id), MAIN_MENU_STATUS_CODE)
    method = registry.get(state)
    method(bot, update)


def get_flight_number(redis, user_id):
    result: bytes = redis.get(f'user:{user_id}:flight_number')

    if result:
        return str(result.decode())
    return result
