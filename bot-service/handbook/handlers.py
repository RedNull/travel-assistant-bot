import random

from telegram import InlineKeyboardMarkup, InlineKeyboardButton

from currencies.handlers import currencies_handler
from airport_scheme.handlers import scheme_handler
from utils import log
from .constants import taxis


def query_handler(bot, update):
    query = update.callback_query
    chat_id = query.message.chat.id
    user_id = query.message.from_user.id
    if query.data in ['taxi_handbooks']:
        return taxi_handbook_handler(bot, chat_id, query.data)

    if query.data in ['currencies_handbooks']:
        return currencies_handler(bot, user_id, chat_id, query.data)

    if query.data in ['airport_schema_handbooks']:
        return scheme_handler(bot, chat_id)


@log
def handbook_handler(bot, update, chat_id=None):
    intro_text = 'Доброе пожаловать в справочник.'
    if not chat_id:
        update.message.reply_text(intro_text)

    custom_keyboard = [
        (InlineKeyboardButton('Такси', callback_data='handbook:taxi_handbooks'),),
        (InlineKeyboardButton('Курсы валют', callback_data='handbook:currencies_handbooks'),),
        (InlineKeyboardButton('Схема аэропорта', callback_data='handbook:airport_schema_handbooks'),),

    ]
    reply_markup = InlineKeyboardMarkup(custom_keyboard, resize_keyboard=False)

    question = 'По каким пунктам вы хотите получить информацию?'
    if chat_id:
        bot.send_message(chat_id, question, reply_markup=reply_markup)
    else:
        update.message.reply_text(question, reply_markup=reply_markup)


def taxi_handbook_handler(bot, chat_id, command):
    random.seed(1)
    taxi = random.choice(taxis)
    message_to_user = ''

    bot.send_message(chat_id=chat_id, text='Ниже список самых дешевых таксопарков в вашем городе:')
    for taxi in taxi['companies']:
        message_to_user += f"""{taxi['title']}: {taxi['phone']}\n"""
    bot.send_message(chat_id=chat_id, text=message_to_user)
