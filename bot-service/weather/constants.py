WEATHER_API_KEY = 'a48a5b6ac659c886bbf6ed7d843fc1af'
QUERY_URL = 'http://api.openweathermap.org/data/2.5/forecast'

CELSUS_SIGN = '\u2103'

WEATHER_ICONS = {
    '01d': '\u2600',  # Clear
    '04d': '\u2601',   # Clouds
    '10d': '\u1F327',   # Rain
}