import json
import logging
from datetime import datetime, date

import requests
from redis import Redis

from utils import log
from . import constants

logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")
LOGGER = logging.getLogger('bot_service:weather')

redis = Redis('redis')


@log
def display_weather(bot, update, chat_id=None):
    chat_id = chat_id if chat_id else update.message.chat_id

    flight_info = json.loads(redis.get(f'user:{chat_id}:flight') or '{}')
    if flight_info:
        arrival_city_name = flight_info['arr_airport']['name']
        arrival_city, country_code = flight_info['arr_airport']['city']['id'].split('_')
    else:
        flight_product = json.loads(
            redis.get(f'user:{chat_id}:product_data') or '{}')
        offer = flight_product.get('offer')
        segment = offer['flights'][0]['segments'][0]

        arrival_city_name = segment['arr']['airport_info']['city']['name']
        arrival_city, country_code = segment['arr']['airport_info']['city']['id'].split('_')

    response = get_weather(arrival_city_name, arrival_city, country_code)

    if response:
        bot.send_message(chat_id, response)
    else:
        bot.send_message(chat_id,
                         f'К сожалению, мы не имеем информации по погоде в {arrival_city_name}')


def get_weather(arrival_city_name, arrival_city, country_code):
    query_settings = {
        'appid': constants.WEATHER_API_KEY,
        'units': 'metric',
        'lang': 'ru',
        'q': f'{arrival_city},{country_code}'
    }
    weather_data = requests.get(constants.QUERY_URL, params=query_settings)
    if weather_data.status_code == 200:
        return get_formatted_weather(weather_data.json().get('list'), arrival_city_name)

    return None


def get_formatted_weather(response, arrival_city_name):
    today = date.today()
    weather_by_days = group_by_date(response)
    final_data = find_highest_temp_per_day(weather_by_days)
    return f'Погода в {arrival_city_name}: {format_data(today, final_data[today])}'


def group_by_date(forecast_list):
    dict_by_date = {}
    for item in forecast_list:
        forecast_day = datetime.strptime(item['dt_txt'].split(' ')[0], '%Y-%m-%d').date()
        dict_by_date.setdefault(forecast_day, []).append(item)
    return dict_by_date


def find_highest_temp_per_day(weather_by_days):
    res_dict = {}
    for day in weather_by_days:
        highest_temp = -274  # Absolute Zero
        for item in weather_by_days[day]:
            if int(item['main']['temp_max']) > highest_temp:
                highest_temp = int(item['main']['temp_max'])
                res_dict.setdefault(day, item)
                res_dict[day] = item
    return res_dict


def format_data(day, weather):
    temperature = weather['main']['temp_max']
    weather_desc = weather['weather'][0]['description']
    weather_icon = constants.WEATHER_ICONS.get(weather['weather'][0]['icon'], '')

    return f'{weather_icon} {weather_desc} {temperature} {constants.CELSUS_SIGN}'
