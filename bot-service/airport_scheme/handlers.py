import json

import redis

redis = redis.Redis(host='redis', port=6379, db=0)


def scheme_handler(bot, chat_id):
    flight = json.loads(redis.get(f'{chat_id}:flight') or '{}')
    if flight:
        arrival_city_name = flight['arr_airport']['city']['name']
    else:
        flight_product = json.loads(redis.get(f'user:{chat_id}:product_data') or '{}')
        offer = flight_product.get('offer')
        segment = offer['flights'][0]['segments'][0]
        arrival_city_name = segment['arr']['airport_info']['city']['name']

    bot.send_message(chat_id,
                     f'Схема аэропорта в городе {arrival_city_name}:')
    with open(f'static/airport_schemas/TSE_scheme.jpg', 'rb') as file:
        bot.send_photo(chat_id=chat_id, photo=file)
