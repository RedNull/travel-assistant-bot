import json
import time
from datetime import datetime

import redis
import requests
import xmltodict

from utils import log
from .constants import CURRENCIES_BY_COUNTRY_CODE

NATIONAL_BANK_URL = 'https://www.nationalbank.kz/rss/get_rates.cfm?fdate='

r = redis.Redis('redis')


@log
def currencies_handler(bot, user_id, chat_id, command):
    nat_bank_currencies = _fetch_kaz_national_bank()
    flight = r.get(f'user:{chat_id}:product_data') or b'{}'
    flight = json.loads(flight.decode())

    text = f"""Курсы валют:
1 EUR = {nat_bank_currencies['EUR']} KZT
1 USD = {nat_bank_currencies['USD']} KZT
1 RUB = {nat_bank_currencies['RUB']} KZT
1 CZK = {nat_bank_currencies['CZK']} KZT
1 KGS = {nat_bank_currencies['KGS']} KZT
"""
    if flight:
        arr_country_id = flight['offer']['flights'][0]['segments'][0]['arr']['airport_info']['city']['country']['id']
        arr_currency = CURRENCIES_BY_COUNTRY_CODE.get(arr_country_id,
                                                      CURRENCIES_BY_COUNTRY_CODE['US'])
        arr_currency = arr_currency[0]

        if arr_currency != 'KZT':
            text = f"Курс валют в стране прибывания: \n" \
                f"1 {arr_currency} = {nat_bank_currencies[arr_currency]} KZT"

    else:
        text += '\n\nИмпортируйте бронь для получения данных по местному курсу валют.'

    bot.send_message(chat_id=chat_id, text=text)


def _fetch_kaz_national_bank():
    url = NATIONAL_BANK_URL + datetime.now().strftime('%d.%m.%Y')

    for _ in range(5):
        response = requests.get(url)
        if response.ok:
            break

        time.sleep(1)

    parsed_response = xmltodict.parse(response.text)
    currency_rate_items = parsed_response['rates']['item']
    return {
        rate['title']: rate['description'] for rate in currency_rate_items
    }
