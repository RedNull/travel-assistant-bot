import json
import os
import re

import psycopg2
import redis
import requests
from telegram import InlineKeyboardButton, \
    InlineKeyboardMarkup, KeyboardButton, ReplyKeyboardMarkup

import constants
import keyboard
import utils

redis = redis.Redis('redis')
BASE_URL = 'https://aviation-edge.com/v2/public'
KEY = '33e489-95195f'
DATABASE_URL = os.environ['DATABASE_URL']


@utils.log
@utils.state_start(constants.REQUEST_CONTACT)
def sign_in(bot, update):
    if update.callback_query:
        chat_id = update.callback_query.message.chat.id
    else:
        chat_id = update.message.chat.id

    reply_markup = ReplyKeyboardMarkup(
        [[KeyboardButton(text="Отправить мой номер", request_contact=True)]],
        resize_keyboard=True, one_time_keyboard=True)
    bot.send_message(chat_id, 'Нам нужен ваш номер телефона для импорта броней',
                     reply_markup=reply_markup)


@utils.log
@utils.state_end(constants.REQUEST_CONTACT)
def sign_in_complete(bot, update):
    phone = update.message.contact.phone_number.replace('+', '')
    product_data = get_product_data_by_phone(phone)
    user_id = update.message.from_user.id

    main_menu_keyboard = keyboard.MainMenuKeyboard()
    main_menu_keyboard.display_menu(bot, update)

    update.message.reply_text('Загрузка перелетов из Chocotravel.com и Aviata.kz ...')

    offer = product_data['offer']

    segment = offer['flights'][0]['segments'][0]

    dep_airport = api_get_airport(segment['dep']['airport'])
    segment['dep']['airport_info'] = dep_airport
    dep_airport_name = dep_airport['name']
    departure_time = segment['dep']['at'][11:16]

    arr_airport = api_get_airport(segment['arr']['airport'])
    segment['arr']['airport_info'] = arr_airport
    arr_airport_name = arr_airport['name']
    arrival_time = segment['arr']['at'][11:16]

    airline_name = constants.AIRLINE_NAME_BY_CODE.get(segment['airline'], segment['airline'])
    flight_number = segment['airline'] + segment['flight_number']

    flight_text = f'✈ Рейс: {dep_airport_name} — {arr_airport_name} ' \
                  f'({airline_name} {flight_number})\n' \
                  f'🕛 Отправление: {departure_time}\n' \
                  f'🕛 Прибытие: {arrival_time}\n' \
                  '✅ Статус: В ожидании'

    filename = get_map_photo_filename(dep_airport, arr_airport, flight_number)

    redis.set(f'user:{user_id}:flight_number', flight_number)
    redis.set(f'user:{user_id}:flight_text', flight_text)
    redis.set(f'user:{user_id}:phone', phone)
    redis.set(f'user:{user_id}:product_data', json.dumps(product_data, default=str))

    bot.send_photo(chat_id=update.message.chat.id, photo=open(filename, 'rb'))
    update.message.reply_text(f'Ваш рейс:\n' + flight_text)


@utils.log
@utils.state_start(constants.ENTER_FLIGHT)
def add_flight(bot, update):
    if update.callback_query:
        chat_id = update.callback_query.message.chat.id
    else:
        chat_id = update.message.chat.id

    bot.send_message(chat_id, 'Введите номер рейса\nПример: KC955')


@utils.log
@utils.state_end(constants.ENTER_FLIGHT)
def received_flight(bot, update):
    update.message.reply_text('Идет поиск рейса')
    text = update.message.text
    user_id = update.message.from_user.id
    match = re.match('(\w{2})([0-9]+)', text)

    if match:
        airline_code, flight_number = match.groups()
        flight = api_get_route(airline_code, flight_number)

        departure_time = flight['departureTime']
        arrival_time = flight['arrivalTime']
        airline = api_get_airline_name(flight['airlineIata'])

        dep_airport = api_get_airport(flight['departureIata'])
        dep_airport_name = dep_airport['name']
        flight['dep_airport'] = dep_airport

        arr_airport = api_get_airport(flight['arrivalIata'])
        arr_airport_name = arr_airport['name']
        flight['arr_airport'] = arr_airport

        flight_text = f'✈ Рейс: {dep_airport_name} — {arr_airport_name} ({airline} {text})\n' \
                      f'🕛 Отправление: {departure_time}\n' \
                      f'🕛 Прибытие: {arrival_time}\n' \
                      '✅ Статус: В ожидании'

        redis.set(f'user:{update.message.chat.id}:flight_number', text)
        redis.set(f'user:{update.message.chat.id}:flight', json.dumps(flight))
        redis.set(f'user:{user_id}:flight_number', text)
        redis.set(f'user:{user_id}:flight_text', flight_text)

        filename = get_map_photo_filename(dep_airport, arr_airport, airline + flight_number)

        bot.send_photo(chat_id=update.message.chat.id, photo=open(filename, 'rb'))
        update.message.reply_text(f'Ваш рейс:\n' + flight_text)
        redis.set(f'{update.message.chat.id}:flight_map_file', filename)

        main_menu_keyboard = keyboard.MainMenuKeyboard()
        main_menu_keyboard.display_menu(bot, update)

        # subscribe(bot, update)
    else:
        update.message.reply_text('Не верный формат рейса')


@utils.log
def show_flight(bot, update):
    user_id = update.message.from_user.id
    key = f'user:{user_id}:flight_text'
    flight_text = redis.get(key) if redis.exists(key) else None
    if flight_text:
        flight_number = redis.get(f'user:{user_id}:flight_number').decode('utf-8')
        bot.send_photo(chat_id=update.message.chat.id,
                       photo=open(f'images/{flight_number}.jpg', 'rb'))
        update.message.reply_text(flight_text.decode('utf-8'))
    else:
        keyboard = [[InlineKeyboardButton("Добавить рейс", callback_data=f'add_flight:{user_id}')]]
        reply_markup = InlineKeyboardMarkup(keyboard)
        update.message.reply_text('У вас нет рейса', reply_markup=reply_markup)


def callback_query_handler(bot, update):
    query = update.callback_query.data.split(':')[0]
    if query == 'add_flight':
        add_flight(bot, update)


def api_get_route(airline, flight_number):
    resp = requests.get(
        f'{BASE_URL}/routes?key={KEY}&airlineIata={airline}&flightNumber={flight_number}')
    data = resp.json()

    if 'error' in data:
        return []

    return resp.json()[0]


def api_get_airline_name(airline):
    resp = requests.get(
        f'{BASE_URL}/airlineDatabase?key={KEY}&codeIataAirline={airline}')
    data = resp.json()

    if 'error' in data:
        return []

    return resp.json()[0]['nameAirline']


def api_get_airport(airport):
    resp = requests.get(
        f'https://api.skypicker.com/locations?term={airport}'
        f'&locale=ru-RU&location_types=airport&limit=1&active_only=true&sort=name')
    data = resp.json()
    return data['locations'][0]


def get_map_url(dep, arr):
    dep_lat, dep_lon = dep['location']['lat'], dep['location']['lon']
    arr_lat, arr_lon = arr['location']['lat'], arr['location']['lon']
    return f'https://maps.googleapis.com/maps/api/staticmap?autoscale=false&size=600x300' \
           f'&maptype=terrain&key=AIzaSyA4ZOGQcdjDy6w0A7SmUd-pTJb1qZH_pNM=&format=png&' \
           f'visual_refresh=true' \
           f'&markers=size:mid%7Ccolor:0xff0000%7Clabel:A%7C{dep_lat},{dep_lon}'\
           f'&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7C{arr_lat},{arr_lon}' \
           f'&path=color:0x0000FF80|weight:5|{dep_lat},{dep_lon}|{arr_lat},{arr_lon}'


def get_map_photo_filename(dep_airport, arr_airport, flight_number):
    map_url = get_map_url(dep_airport, arr_airport)
    r = requests.get(map_url, allow_redirects=True)
    file_name = f'images/{flight_number}.jpg'
    if r.status_code == 200:
        with open(file_name, 'wb') as f:
            for chunk in r.iter_content(1024):
                f.write(chunk)

    return file_name


def get_product_data_by_phone(phone):
    conn = psycopg2.connect(DATABASE_URL)
    cursor = conn.cursor()
    cursor.execute(f"select a.* from ("
                   f"select p.data, p.created_at from choco_kz.orders_product p "
                   f"left join choco_kz.orders_order o on p.order_id=o.id  "
                   f"where o.contact_phone='+{phone}' and o.status='PROCESS_OK' "
                   f"union all "
                   f"select p.data, p.created_at from kz.orders_product p "
                   f"left join choco_kz.orders_order o on p.order_id=o.id  "
                   f"where o.contact_phone='+{phone}' and o.status='PROCESS_OK' "
                   f") a order by a.created_at LIMIT 1")
    result = cursor.fetchall()
    conn.close()
    return result[0][0] if result else None
