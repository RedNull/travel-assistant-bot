import json
import time

import redis
from telegram import InlineKeyboardButton, InlineKeyboardMarkup

from flight.flight import sign_in

r = redis.Redis('redis')

TOTAL = 0
TAX = 0


def query_handler(bot, update):
    if 'passenger' in update.callback_query.data:
        passenger = update.callback_query.data.split(':')[1]

        custom_keyboard = [
            (InlineKeyboardButton('Согласиться', callback_data=f'refund:agree:{passenger}'),),
        ]

        difference = float(TOTAL) - float(TAX)
        bot.send_message(chat_id=update.callback_query.message.chat.id,
                         text=f'Стоимость заказа: {TOTAL} KZT. \n'
                         f'Общая сумма удержания: {TAX} KZT, с учетом штрафа и сервисного сбора за процедуру возврата. \n'
                         f'Сумма возврата:  {difference}')

        reply_markup = InlineKeyboardMarkup(custom_keyboard, resize_keyboard=False)
        bot.send_message(chat_id=update.callback_query.message.chat.id,
                         text='Подтвердите Ваше согласие на возврат средств',
                         reply_markup=reply_markup)

    if 'agree' in update.callback_query.data:
        passenger = update.callback_query.data.split(':')[1]

        bot.send_message(chat_id=update.callback_query.message.chat.id,
                         text=f'Пытаемся оформить возврат для пассажира: {passenger}.\n'
                         f'Пожалуйста подождите....')
        time.sleep(5)
        bot.send_message(chat_id=update.callback_query.message.chat.id,
                         text=f'Возврат успешно выполнен. '
                         f'Деньги поступят на Ваш счет в течении 24 часов.')


def refund_handler(bot, update):
    user_id = update.message.from_user.id
    product_data_key = f'user:{user_id}:product_data'

    if r.exists(product_data_key):
        product_data = r.get(product_data_key) or b'{}'
        product_data = json.loads(product_data)
        global TOTAL, TAX
        TOTAL = f"{product_data['offer']['pricing'][0]['base']}"
        TAX = f"{product_data['offer']['pricing'][0]['taxes']}"

        custom_keyboard = []

        for passenger in product_data['passengers']:
            custom_keyboard.append(
                (InlineKeyboardButton(
                    passenger['full_name'],
                    callback_data=f'refund:passenger:{passenger["full_name"]}'),),
            )

        reply_markup = InlineKeyboardMarkup(custom_keyboard, resize_keyboard=False)
        update.message.reply_text('Выберите пассажира, для которого требуется оформить возврат.',
                                  reply_markup=reply_markup)

    else:
        update.message.reply_text('У Вас нет импортированного заказа. '
                                  'Импортируйте заказ по номеру телефона чтобы продолжить')
        sign_in(bot, update)
