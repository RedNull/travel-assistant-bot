import json
import random
import time

import redis
from telegram import InlineKeyboardButton, InlineKeyboardMarkup

from flight.flight import sign_in

r = redis.Redis('redis')

PASSENGER = ''


def query_handler(bot, update):
    if 'passenger' in update.callback_query.data:
        passenger = update.callback_query.data.split(':')[1]

        custom_keyboard = [
            (InlineKeyboardButton('Оплатить', callback_data=f'change:paid:{passenger}'),),
        ]

        bot.send_message(chat_id=update.callback_query.message.chat.id,
                         text=f'Стоимость заказа дороже текущего:  '
                         f'{random.randint(1000, 5000)} KZT. \n')

        reply_markup = InlineKeyboardMarkup(custom_keyboard, resize_keyboard=False)
        bot.send_message(chat_id=update.callback_query.message.chat.id,
                         text='Оплатите разницу двух рейсов.',
                         reply_markup=reply_markup)

    if 'paid' in update.callback_query.data:
        passenger = update.callback_query.data.split(':')[1]

        bot.send_message(chat_id=update.callback_query.message.chat.id,
                         text=f'Пытаемся выкупить рейс для пассажира: {passenger}.\n'
                         f'Пожалуйста подождите....')
        time.sleep(5)
        bot.send_message(chat_id=update.callback_query.message.chat.id,
                         text=f'Билет успешно обменен. Новая маршрут-квитанция была выслана '
                         f'на ваш электронный адрес.')


def change_handler(bot, update):
    user_id = update.message.from_user.id
    product_data_key = f'user:{user_id}:product_data'

    if r.exists(product_data_key):
        product_data = r.get(product_data_key) or b'{}'
        product_data = json.loads(product_data)

        custom_keyboard = []

        for passenger in product_data['passengers']:
            custom_keyboard.append(
                (InlineKeyboardButton(
                    passenger['full_name'],
                    callback_data=f'change:passenger:{passenger["full_name"]}'),),
            )

        reply_markup = InlineKeyboardMarkup(custom_keyboard, resize_keyboard=False)
        update.message.reply_text('Выберите пассажира, для которого требуется обмен билета.',
                                  reply_markup=reply_markup)

    else:
        update.message.reply_text('У Вас нет импортированного заказа. '
                                  'Импортируйте заказ по номеру телефона чтобы продолжить')
        sign_in(bot, update)
