import random

from telegram import Bot, ReplyKeyboardMarkup, InlineKeyboardButton, InlineKeyboardMarkup

from handbook import handlers as info_handler
from notifications.const import NotificationCode as Code
from todo import handlers as todo_handler
from weather.handlers import display_weather


def boarding_tip(*args, **kwargs):
    if random.randint(False, True):
        return 'Не забудьте размяться перед посадкой'


def flight_end_tip(bot, chat_id, flight_number):
    display_weather(bot, None, chat_id)
    info_handler.handbook_handler(bot, update=None, chat_id=chat_id)


def airport_trip_tip(bot: Bot, chat_id, flight_number):
    todo_handler.todo(bot, update=None, chat_id=chat_id)
    response_options = (
        'Не забудьте проверить все документы',
    )
    return random.choice(response_options)


def reg_tip(bot: Bot, chat_id, flight_number):
    # online_checkin_handler(bot, update=None, chat_id=chat_id)
    custom_keyboard = [
        (InlineKeyboardButton('Онлайн регистрация', callback_data='checkin:init'),)
    ]
    reply_markup = InlineKeyboardMarkup(custom_keyboard, resize_keyboard=False)
    bot.send_message(chat_id, 'Зарегестрироваться на рейс', reply_markup=reply_markup)


STATUS_CALLBACK_MAP = {
    Code.BOARDING: boarding_tip,
    Code.FLIGHT_END: flight_end_tip,
    Code.AIRPORT_TRIP: airport_trip_tip,
    Code.REGISTRATION: reg_tip
}
