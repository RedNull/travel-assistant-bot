import json
import logging
from itertools import cycle

from redis import Redis
from telegram import User, Message, Bot, ReplyKeyboardMarkup

import utils
from notifications import status_checker
from notifications.reminder import STATUS_CALLBACK_MAP
from notifications.status_checker import get_status
from utils import log
from .const import NotificationCode, NOTIFICATIONS


logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")
LOGGER = logging.getLogger('bot_service:notifications')

STATUS_CHECK_INTERVAL = 8
REDIS_KEY = 'notify'

redis = Redis('redis')
notifications_pool = json.loads(redis.get(REDIS_KEY) or '{}')

LOGGER.info(f'Redis keys: {redis.keys()}')


@log
def notify(bot, update):
    custom_keyboard = []
    reply_markup = ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
    update.message.reply_text('Настройки уведомлений:', reply_markup=reply_markup)


@log
def subscribe(bot, update):
    message: Message = update.message
    user: User = message.from_user

    flight_number = utils.get_flight_number(redis, user.id)
    if not flight_number:
        update.message.reply_text('Рейс не выбран')
        return

    if user.id in notifications_pool.get(flight_number, {}).get('chats', []):
        return unsubscribe(bot, update)

    notifications_pool.setdefault(flight_number, {})
    notifications_pool[flight_number].setdefault('status', NotificationCode.NEW)
    notifications_pool[flight_number].setdefault('chats', []).append(
        message['chat']['id'])
    redis.set(REDIS_KEY, json.dumps(notifications_pool))
    update.message.reply_text(f'Вы подписаны на уведомления о {flight_number}')


@log
def unsubscribe(bot, update):
    message: Message = update.message
    user: User = message.from_user

    flight_number = utils.get_flight_number(redis, user.id)
    if not flight_number:
        update.message.reply_text('Рейс не выбран')
        return

    try:
        chats: set = notifications_pool[flight_number]['chats']
        chats.remove(user.id)
    except KeyError:
        update.message.reply_text(f'Вы не подписаны на уведомления {flight_number}')
        return

    notifications_pool[flight_number]['status'] = NotificationCode.NEW  # for demo
    status_checker.get_status_code = cycle(list(NOTIFICATIONS.keys()))  # for demo

    redis.set(REDIS_KEY, json.dumps(notifications_pool))
    update.message.reply_text(f'Вы отписались от уведомлений о {flight_number}')


def check_statuses(bot, job):
    for flight_number, info in notifications_pool.items():
        if not len(info['chats']):
            continue

        LOGGER.info(f'Check status: {flight_number}')

        current_status = notifications_pool[flight_number]['status']
        if current_status in NotificationCode.ERROR_CODES:
            continue

        status_code, status_message = get_status(info['chats'][0], current_status)
        if status_code == info['status']:
            continue

        info['status'] = status_code
        LOGGER.info(f'Status changed: {flight_number}')

        additional_tip_func = STATUS_CALLBACK_MAP.get(status_code)
        for chat_id in info['chats']:
            redis.set(REDIS_KEY, json.dumps(notifications_pool))
            send_notification(bot, chat_id, status_message)
            if additional_tip_func:
                message = additional_tip_func(bot, chat_id, flight_number)
                if message:
                    send_notification(bot, chat_id, message)


@log
def send_notification(bot: Bot, chat_id, message):
    bot.send_message(chat_id, f'{message}')
