import json
import random
from datetime import datetime, timedelta
from itertools import cycle

from redis import Redis

from notifications.const import NOTIFICATIONS, NotificationCode

get_gate = cycle('ABCDEFGHIJ')
get_terminal = cycle('123456')

get_status_code = cycle(list(NOTIFICATIONS.keys()))

redis = Redis('redis')


def get_status(user_id, current_status):
    status_code = next(get_status_code)
    if random.randint(0, 10) == 0:
        status_code = NotificationCode.FLIGHT_CANCEL

    if current_status >= NotificationCode.ERROR_GROUP or \
            current_status == NotificationCode.END_STATUS:
        return current_status, NOTIFICATIONS[current_status]

    flight_product = json.loads(redis.get(f'user:{user_id}:product_data') or '{}')
    offer = flight_product.get('offer')

    flight = json.loads(redis.get(f'user:{user_id}:flight') or '{}')

    message = NOTIFICATIONS[status_code]

    if status_code == NotificationCode.FLIGHT_DELAY:
        date = datetime.now() + timedelta(days=1)
        message = message.format(time=date.replace(microsecond=0))

    if status_code == NotificationCode.AIRPORT_TRIP:
        if offer:
            departure_time = datetime.fromisoformat(
                offer['flights'][0]['segments'][0]['dep']['at'])
            departure_time = str(departure_time.time())

        else:
            departure_time = flight['departureTime']
        message = message.format(time=departure_time)

    if status_code == NotificationCode.FLIGHT:
        duration = '100 часов'
        if offer:
            segment = offer['flights'][0]['segments'][0]
            dep = segment['dep']['at']
            arr = segment['arr']['at']
            duration = datetime.fromisoformat(arr) - datetime.fromisoformat(dep)

        message = message.format(time=duration)

    if status_code == NotificationCode.FLIGHT_END:
        if offer:
            segment = offer['flights'][0]['segments'][0]
            city_name = segment['arr']['airport_info']['city']['name']
        else:
            city_name = flight['arr_airport']['city']['name']
        message = message.format(city=city_name)

    if status_code == NotificationCode.BOARDING:
        message = message.format(terminal=next(get_terminal), gate=next(get_gate))

    return status_code, message
