class NotificationCode:
    NEW = 0
    AIRPORT_TRIP = 1
    REGISTRATION = 2
    BOARDING = 3
    REGISTRATION_END = 4
    FLIGHT = 5
    FLIGHT_END = 6

    END_STATUS = FLIGHT_END

    ERROR_GROUP = 90
    FLIGHT_DELAY = 97
    FLIGHT_CANCEL = 98

    ERROR_CODES = (
        FLIGHT_DELAY,
        FLIGHT_CANCEL
    )

    SUCCESS_CODES = (
        AIRPORT_TRIP,
        REGISTRATION,
        BOARDING,
        FLIGHT,
    )


INFO_SIGN = '\U00002139'
ERROR_SIGN = '\U000026A0'

NOTIFICATIONS = {
    NotificationCode.NEW: ' Статус в обработке',
    NotificationCode.REGISTRATION: INFO_SIGN + ' Регистрация началась',
    NotificationCode.AIRPORT_TRIP: INFO_SIGN + ' Рекомендуем заблаговременно прибыть в аэропорт.\n'
                                               'Вылет в {time}',
    NotificationCode.BOARDING: INFO_SIGN + ' Посадка началась\n'
                                           'Tерминал: {terminal} выход: {gate}',

    NotificationCode.REGISTRATION_END: INFO_SIGN + ' Регистрация на рейс закончена',
    NotificationCode.FLIGHT: INFO_SIGN + ' Приятного полета\nВремя в пути: {time}',
    NotificationCode.FLIGHT_END: INFO_SIGN + '  Добро пожаловать в {city}',

    NotificationCode.FLIGHT_DELAY: ERROR_SIGN + ' Рейс перенесен вылет: {time}',
    NotificationCode.FLIGHT_CANCEL: ERROR_SIGN + ' Рейс отменен',
}