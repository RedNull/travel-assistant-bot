from collections import namedtuple

from telegram import ReplyKeyboardMarkup

import utils
from checkin.handlers import online_checkin_handler
from constants import MAIN_MENU_STATUS_CODE
from flight import flight
from handbook.handlers import handbook_handler
from notifications import handler as notify_handler
from refund.handlers import refund_handler
from todo import handlers as todo_handlers

KeyboardBundle = namedtuple('KeyboardBundle', 'text method')


def build_keyboard(markup):
    keyboard = []
    for row_buttons in markup:
        keyboard.append([btn.text for btn in row_buttons])

    return ReplyKeyboardMarkup(keyboard, resize_keyboard=True)


def handle_menu(bot, update, markup):
    for row_buttons in markup:
        for button in row_buttons:
            if button.text == update.message.text:
                button.method(bot, update)
                return


class MainMenuKeyboard:
    STATE_CODE = MAIN_MENU_STATUS_CODE
    MARKUP = (
        (
            KeyboardBundle('Импортировать с Aviata/Chocotravel', flight.sign_in),
        ),
        (
            KeyboardBundle('Добавить рейс', flight.add_flight),
            KeyboardBundle('Показать рейс', flight.show_flight),
        ),
        (
            (KeyboardBundle('Возврат билета', refund_handler)),
        ),
        (
            KeyboardBundle('Онлайн регистрация', online_checkin_handler),
            KeyboardBundle('Справочник', handbook_handler),
        ),
        (
            KeyboardBundle('Чек-лист', todo_handlers.todo),
            KeyboardBundle('Уведомления', notify_handler.subscribe),
        ),
    )

    @staticmethod
    @utils.state_start(STATE_CODE)
    def display_menu(bot, update):
        reply_markup = build_keyboard(MainMenuKeyboard.MARKUP)
        update.message.reply_text('Меню:', reply_markup=reply_markup)

    @staticmethod
    @utils.state_end(STATE_CODE)
    def handle_menu(bot, update):
        handle_menu(bot, update, MainMenuKeyboard.MARKUP)
