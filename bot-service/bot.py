import datetime
import logging
import os

from telegram.ext import Dispatcher, JobQueue
from telegram.ext import MessageHandler, Filters
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler

import keyboard
from change.handlers import change_handler
from checkin.handlers import online_checkin_handler
from currencies.handlers import currencies_handler
from flight import flight
from handbook.handlers import handbook_handler
from notifications import handler as notify_handler
from query_handler import query_handler
from refund.handlers import refund_handler
from todo.handlers import todo
from utils import log
from utils import perform_state_handlers
from weather.handlers import display_weather

logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")
LOGGER = logging.getLogger('bot_service')

boot_time = datetime.datetime.now()

TOKEN = os.environ['TOKEN']
updater = Updater(TOKEN)
dp: Dispatcher = updater.dispatcher

main_menu_keyboard = keyboard.MainMenuKeyboard()


def main():
    LOGGER.info('Bot started')
    LOGGER.info('Start time: {}'.format(boot_time))

    # main
    dp.add_handler(CommandHandler('start', start))
    dp.add_handler(CommandHandler('upt', get_uptime))

    # flight info
    dp.add_handler(CommandHandler('sign_in', flight.sign_in))
    dp.add_handler(CommandHandler('add_flight', flight.add_flight))
    dp.add_handler(CommandHandler('show_flight', flight.show_flight))
    dp.add_handler(MessageHandler(Filters.text, perform_state_handlers))
    dp.add_handler(MessageHandler(Filters.contact, perform_state_handlers))

    # online checkin
    dp.add_handler(CommandHandler('online_checkin', online_checkin_handler))
    dp.add_handler(CommandHandler('handbooks', handbook_handler))
    dp.add_handler(CommandHandler('currencies', currencies_handler))

    dp.add_handler(CommandHandler('refund', refund_handler))
    dp.add_handler(CommandHandler('change', change_handler))

    # notifications

    queue = JobQueue(dp.bot)
    queue.run_repeating(notify_handler.check_statuses, notify_handler.STATUS_CHECK_INTERVAL)
    queue.start()

    # dp.add_handler(CommandHandler('notify', NotifyMenuKeyboard().display_menu))
    dp.add_handler(CommandHandler('subscribe', notify_handler.subscribe))
    dp.add_handler(CommandHandler('unsubscribe', notify_handler.unsubscribe))
    # todo_list
    dp.add_handler(CommandHandler('todo', todo))
    dp.add_handler(CommandHandler('weather', display_weather))

    dp.add_handler(CommandHandler('init', main_menu_keyboard.display_menu))

    dp.add_handler(CallbackQueryHandler(query_handler))

    # Start the Bot
    updater.start_polling()

    # Run bot
    LOGGER.info('Load completed')
    updater.idle()


@log
def start(bot, update):
    main_menu_keyboard.display_menu(bot, update)


@log
def get_uptime(bot, update):
    update.message.reply_text('Uptime: {}'.format(datetime.datetime.now() - boot_time))


if __name__ == '__main__':
    main()

"""
def template(bot, update):
    message: Message = update.message
    user: User = message.from_user
"""
