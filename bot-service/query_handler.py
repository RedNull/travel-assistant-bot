import logging

from flight import flight
from checkin import handlers as checkin
from handbook import handlers as handbook
from todo import handlers as todo
from change import handlers as change
from refund import handlers as refund

logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")
LOGGER = logging.getLogger('query_handler')


def query_handler(bot, update):
    query = update.callback_query.data.split(':')
    LOGGER.info(query)

    module_name = query[0]
    update.callback_query.data = ''.join(query[1:])

    if module_name == 'flight':
        flight.callback_query_handler(bot, update)

    if module_name == 'checkin':
        checkin.query_handler(bot, update)

    if module_name == 'handbook':
        handbook.query_handler(bot, update)

    if module_name == 'todo':
        todo.query_handler(bot, update)

    if module_name == 'refund':
        update.callback_query.data = ':'.join(query[1:])
        refund.query_handler(bot, update)

    if module_name == 'change':
        update.callback_query.data = ':'.join(query[1:])
        change.query_handler(bot, update)
